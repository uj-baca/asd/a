import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

public class Source {
	private static Vertex[][] data;
	static PriorityQueue<Vertex> queue;
	private static short m;
	private static short n;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int z = scanner.nextInt();
		for (int i = 0; i < z; i++) {
			m = scanner.nextShort();
			n = scanner.nextShort();

			data = new Vertex[m][n];
			for (short j = 0; j < m; j++) {
				for (short k = 0; k < n; k++) {
					data[j][k] = new Vertex(j, k, scanner.nextShort());
				}
			}

			byte p = scanner.nextByte();
			for (byte j = 0; j < p; j++) {
				int x = scanner.nextShort() - 1;
				int y = scanner.nextShort() - 1;
				if (j != 0) {
					reset();
				}
				queue = new PriorityQueue<>(m * n, Comparator.comparingInt(v -> v.summaryCosts));
				data[x][y].summaryCosts = -data[x][y].enteringCosts;
				queue.add(data[x][y]);
				while (!queue.isEmpty()) {
					Vertex curr = queue.poll();
					if (curr.x > 0) {
						data[curr.x - 1][curr.y].checkNode(curr);
					}
					if (curr.x + 1 < m) {
						data[curr.x + 1][curr.y].checkNode(curr);
					}
					if (curr.y > 0) {
						data[curr.x][curr.y - 1].checkNode(curr);
					}
					if (curr.y + 1 < n) {
						data[curr.x][curr.y + 1].checkNode(curr);
					}
				}
				System.out.println(data[m - 1][0].summaryCosts + ", " + (data[0][n - 1].summaryCosts + data[x][y].enteringCosts));
			}
		}
		scanner.close();
	}

	private static void reset() {
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				data[i][j].summaryCosts = Integer.MAX_VALUE;
			}
		}
	}
}

class Vertex {
	final short x;
	final short y;
	int summaryCosts = Integer.MAX_VALUE;
	int enteringCosts;

	Vertex(short x, short y, short enteringCosts) {
		this.x = x;
		this.y = y;
		this.enteringCosts = enteringCosts;
	}

	void checkNode(Vertex other) {
		if (summaryCosts > other.summaryCosts + enteringCosts) {
			summaryCosts = other.summaryCosts + enteringCosts;
			Source.queue.add(this);
		}
	}
}
